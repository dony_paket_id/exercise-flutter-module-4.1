import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  static const Color white = Colors.white;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Container(
          color: Colors.pink,
          child: SafeArea(
            child: Center(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Image.asset("images/plane.png"),
                    const SizedBox(
                      // kurang const
                      height: 40,
                    ),
                    const Text(
                      "TRAVEL APP",
                      style: TextStyle(
                          color: white,
                          fontSize: 20.0,
                          letterSpacing: 2.0,
                          fontWeight: FontWeight.bold // kurang ini
                          ),
                    ),
                    const SizedBox(
                      height: 40,
                    ),
                    iconWithOutlinedButton(Icons.hotel_rounded, "Stays"),
                    iconWithOutlinedButton(Icons.flight, "Flights"),
                    iconWithOutlinedButton(Icons.car_rental_rounded, "Cars"),
                    iconWithTextButton(
                        Icons.local_taxi_rounded, "All-inclusive Vacations"),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget iconWithOutlinedButton(IconData icon, String labelPlusTextToPrint) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(
          icon,
          color: Colors.white,
        ),
        const SizedBox(
          width: 20,
        ),
        OutlinedButton(
          onPressed: () {
            print(labelPlusTextToPrint);
          },
          child: Text(
            labelPlusTextToPrint,
            style: const TextStyle(
              color: Colors.white,
            ),
          ),
          style: OutlinedButton.styleFrom(
            side: const BorderSide(
              color: Colors.white,
              width: 2.0,
            ),
            minimumSize: const Size(
              200.0,
              40.0,
            ),
          ),
        ),
      ],
    );
  }

  Widget iconWithTextButton(IconData icon, String labelPlusTextToPrint) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(
          icon,
          color: Colors.white,
        ),
        const SizedBox(
          width: 20,
        ),
        TextButton(
          onPressed: () {
            print(labelPlusTextToPrint);
          },
          child: Text(
            labelPlusTextToPrint,
            style: const TextStyle(
              color: Colors.pink,
            ),
          ),
          style: TextButton.styleFrom(
            backgroundColor: Colors.white,
            minimumSize: const Size(
              200.0,
              40.0,
            ),
          ),
        ),
      ],
    );
  }
}
